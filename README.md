# Wrapper for Setting up Zookeeper Cluster in Cloud Environment

## Description

[Apache Zookeeper](https://zookeeper.apache.org) is widely used to build distributed systems.
However, setting up and maintaining Zookeeper cluster needs additional [administration work](https://zookeeper.apache.org/doc/trunk/zookeeperAdmin.html).
In the current world on cloud, Zookeeper should be able to be deployed on any cloud environment on demand with minimal manual work.

Before Zookeeper 3.5.0, all cluster members are hard coded in the config file, which makes Zookeeper hard to maintain in a cloud environment.
Scaling up and down horizontally for Zookeeper cluster needs "rolling restarts".

After Zookeeper 3.5.0, dynamic configuration was introduced in Zookeeper.
Instead of "rolling restarts", a relatively small manual administration work is needed to modify the cluster members according to cluster topology changes.
[https://zookeeper.apache.org/doc/trunk/zookeeperReconfig.html](https://zookeeper.apache.org/doc/trunk/zookeeperReconfig.html)

This library is built to detect the cluster topology change automatically and perform the configuration change without human involvement.
It allows users to start a Zookeeper instance by invoking a method call.
The cluster topology settings of Zookeeper can be passed as parameters.
You can get the initial members of the cluster by invoking infrastructure level services or hard code them in a config file.
Also it allows users to start multiple Zookeeper instance locally to form a Zookeeper Cluster for testing.

### When a new node is added to the cluster

Let's say there is a Zookeeper cluster contains nodes A, B, and C. Now node D is booting up.

If a dynamic settings provider is used, node D will contain the setting of (A, B, C, D).
And node D will join the cluster using topology (A, B, C, D).

If a static settings provider is used, node D will still have the setting of (A, B, C).
Since node D find itself is not in the settings, it will add it self to the settings and will join the cluster using topology (A, B, C, D)

However, in A, B, and C, their settings and actual configs are only (A, B, C).
After node D is up, the config will by synchronized from A, B, C.
Now the actual config for D is still (A, B, C).
Since node D found that itself was not in the cluster config, it will add itself to the cluster using dynamic config feature.
After that, node A, B, C, and D will all have the correct config of (A, B, C, D).

### When a node is removed from the cluster

Let's say node C is removed from the cluster.

Each node in the cluster will initiate a heartbeat connection to the rest of cluster members in the config.
If node C is removed permanently, after a the heartbeat interval, the leader of the cluster will remove node C using dynamic config feature.

## Usage

Add the following dependency to your project dependency

sbt

```scala
libraryDependencies += "com.ebay.zookeeper" % "zookeeperlib_2.11" % "0.2.0-SNAPSHOT"
```

maven

```xml
<dependency>
    <groupId>com.ebay.zookeeper</groupId>
    <artifactId>zookeeperlib_2.11</artifactId>
    <version>0.2.0-SNAPSHOT</version>
</dependency>
```

To start a Zookeeper instance, call the following method in your code.

```scala

AutoScalingZkServer.start(clusterSettings)

// for testing, you can pass the host and ports for the current instance
AutoScalingZkServer.start(mySelf, clusterSettings)

// The signature of the methods
def start(clusterSettings: ClusterSettings): Unit
def start(self: QuorumInfo, clusterSettings: ClusterSettings): Unit

// The definition of ClusterSettings
case class ClusterSettings(servers: List[QuorumInfo], persistDir: File, initialDelay: Long, heartbeatInterval: Long)

// The definition of QuorumInfo
case class QuorumInfo(host: InetAddress,
                      quorumPort: Int = QuorumInfo.DEFAULT_QUORUM_PORT,
                      electionPort: Int = QuorumInfo.DEFAULT_ELECTION_PORT,
                      clientPort: Int = QuorumInfo.DEFAULT_CLIENT_PORT,
                      learnerType: LearnerType = LearnerType.PARTICIPANT)

```

### Cluster Settings

`persistDir` is the directory used to store Zookeeper configuration files and data files.

`initialDelay` is the duration after which the heartbeat starts in milliseconds. Usually it is set greater than the total deployment time.

`heartbeatInterval` is the interval between heartbeats in milliseconds.

`servers` are the members in the cluster.

`default_quorum_port` the quorum port will be applied to all hosts by default

`default_election_port` the election port will be applied to all hosts by default

`default_client_port` the client port will be applied to all hosts by default

___To setup a Zookeeper cluster in cloud environment, all members in the cluster should open the same set of ports. Therefore overrides `default_xxxx_port` is recommended___

___For testing cluster behavior in the single node, individual port overriding is also supported in the host section to avoid port conflict. See [this](src/test/resources/application.conf)___

### Zookeeper Version

The zookeeper version used in this library is ___3.5.1-alpha___. For any clients, please use the version that is compatible to Zookeeper 3.5.0

## Local Demo

Open 5 consoles under the project directory.

Start up 3 Zookeeper instances to form the initial cluster:

```sh
console1 > sbt "test:run 2780 2783 2791"
# select the main class com.ebay.zookeeper.ZookeeperServer

console2 > sbt "test:run 2781 2784 2792"
# select the main class com.ebay.zookeeper.ZookeeperServer

console3 > sbt "test:run 2782 2785 2793"
# select the main class com.ebay.zookeeper.ZookeeperServer
```

Start the Zookeeper client to connect to the cluster:

```sh
console5 > sbt "test:run"
# select the main class com.ebay.zookeeper.Client
```

Add a new node to the cluster:

```sh
console4 > sbt "test:run 2786 2787 2794"
# select the main class com.ebay.zookeeper.ZookeeperServer
```

Remove a node from cluster:

Kill any process among console 1 to 4

You can monitor the output on the client side.
