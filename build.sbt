//noinspection ScalaStyle
scalaVersion in ThisBuild := "2.11.7"

name := "zookeeperLib"

organization := "com.ebay.zookeeper"

libraryDependencies ++= Seq(
  "com.typesafe" % "config" % "1.2.1",
  "commons-io" % "commons-io" % "2.5",
  "org.apache.commons" % "commons-lang3" % "3.4",
  "org.apache.zookeeper" % "zookeeper" % "3.5.1-alpha" exclude("org.slf4j", "slf4j-log4j12"),
  "org.apache.curator" % "curator-framework" % "3.2.0",
  "ch.qos.logback" % "logback-classic" % "1.1.7" % "test"
)
