// Copyright (C) 2011-2012 the original author or authors.
// See the LICENCE.txt file distributed with this work for additional
// information regarding copyright ownership.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package com.ebay.zookeeper;

import org.apache.zookeeper.server.admin.AdminServer;
import org.apache.zookeeper.server.persistence.FileTxnSnapLog;
import org.apache.zookeeper.server.quorum.QuorumPeer;
import org.apache.zookeeper.server.quorum.QuorumPeerConfig;
import org.apache.zookeeper.server.quorum.QuorumPeerMain;
import org.apache.zookeeper.server.quorum.QuorumStats;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.util.Collections;
import java.util.Map;

public class ExtendedQuorumPeerMain extends QuorumPeerMain {
  private static final Logger LOG = LoggerFactory.getLogger(ExtendedQuorumPeerMain.class);

  public synchronized boolean isLeader() {
    return quorumPeer != null && quorumPeer.getServerState().equals(QuorumStats.Provider.LEADING_STATE);
  }

  public synchronized Map<Long, QuorumPeer.QuorumServer> getAllMembers() {
    if (quorumPeer == null) {
      return Collections.emptyMap();
    } else {
      return quorumPeer.getView();
    }
  }

  public void start(File configFile) {
    try {
      initializeAndRun(new String[] {configFile.getAbsolutePath()});
    } catch (IllegalArgumentException e) {
      LOG.error("Invalid arguments, exiting abnormally", e);
      System.exit(2);
    } catch (QuorumPeerConfig.ConfigException e) {
      LOG.error("Invalid config, exiting abnormally", e);
      System.exit(2);
    } catch (FileTxnSnapLog.DatadirException e) {
      LOG.error("Unable to access datadir, exiting abnormally", e);
      System.exit(3);
    } catch (AdminServer.AdminServerException e) {
      LOG.error("Unable to start AdminServer, exiting abnormally", e);
      System.exit(4);
    } catch (Exception e) {
      LOG.error("Unexpected exception, exiting abnormally", e);
      System.exit(1);
    }
    LOG.info("Exiting normally");
    System.exit(0);
  }
}
