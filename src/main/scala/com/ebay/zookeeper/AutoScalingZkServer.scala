// Copyright (C) 2011-2012 the original author or authors.
// See the LICENCE.txt file distributed with this work for additional
// information regarding copyright ownership.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package com.ebay.zookeeper

import java.io.File
import java.net.InetAddress
import java.nio.charset.Charset
import java.util.concurrent.{Executors, TimeUnit}

import com.ebay.zookeeper.client.CuratorFrameworkFactoryWrapper
import com.ebay.zookeeper.util.QuorumUtils
import org.apache.commons.io.FileUtils
import org.apache.commons.lang3.text.StrSubstitutor

import scala.collection.JavaConversions._
import scala.io.Source

object AutoScalingZkServer {

  import QuorumUtils._

  lazy val myAddress = InetAddress.getLocalHost
  val checkClusterReadyInterval = 2000

  def start(clusterSettings: ClusterSettings): Unit = start(QuorumInfo(myAddress), clusterSettings)

  def start(self: QuorumInfo, clusterSettings: ClusterSettings): Unit = {
    // check whether I am in the configured servers
    val addMyself = !clusterSettings.servers.exists(_.serverId == self.serverId)

    // obtain the actual servers
    val serverConfigStrings = if (addMyself) {
      self.configString :: clusterSettings.servers.map(_.configString)
    } else {
      clusterSettings.servers.map(_.configString)
    }

    // create the static and dynamic config files
    val configFile = prepareConfigFile(self.serverId, serverConfigStrings, clusterSettings.persistDir)

    val quorum = new ExtendedQuorumPeerMain
    // start the actual zookeeper server thread
    val serverThread = new Thread {
      override def run(): Unit = quorum.start(configFile)
    }
    serverThread.start()

    var members = quorum.getAllMembers
    while (members.isEmpty) {
      Thread.sleep(checkClusterReadyInterval)
      members = quorum.getAllMembers
    }
    // check if really need to add myself to the config
    // 1. node not in the config comes
    // 2. node in the config comes back
    if (addMyself || !members.exists(_._2.serverId == self.serverId)) {
      addQuorumToCluster(self)
    }

    // register heartbeat to monitor the cluster status
    val scheduler = Executors.newScheduledThreadPool(1)
    scheduler.scheduleWithFixedDelay(
      new ClusterMonitor(quorum, self),
      clusterSettings.initialDelay,
      clusterSettings.heartbeatInterval,
      TimeUnit.MILLISECONDS
    )
  }

  private def prepareConfigFile(serverId: Int, serverConfigStrings: List[String], persistDir: File) = {
    val configDir = new File(persistDir, s"conf/server_$serverId")
    if (configDir.exists()) {
      FileUtils.deleteQuietly(configDir);
    }
    configDir.mkdirs()
    val dynamicConfigFile = new File(configDir, "zoo.cfg.dynamic")
    FileUtils.writeLines(dynamicConfigFile, serverConfigStrings)
    // compose the config file
    val templateString = Source.fromInputStream(
      getClass.getClassLoader.getResourceAsStream("zoo.cfg.template")
    ).mkString
    val valueMap = Map(
      "dataDir" -> prepareDataDirectory(serverId, persistDir).getAbsolutePath,
      "dynamicConfigFile" -> dynamicConfigFile.getAbsolutePath
    )
    val cfgFile = new File(configDir, "zoo.cfg")
    FileUtils.write(cfgFile, StrSubstitutor.replace(templateString, valueMap))
    cfgFile
  }

  private def prepareDataDirectory(serverId: Int, persistDir: File) = {
    val dataDir = new File(persistDir, s"data/server_$serverId")
    if (dataDir.exists()) {
      FileUtils.deleteQuietly(dataDir)
    }
    dataDir.mkdirs()
    FileUtils.write(new File(dataDir, "myid"), serverId.toString, Charset.forName("utf8"))
    dataDir
  }

  private def addQuorumToCluster(quorumInfo: QuorumInfo) = {
    import CuratorFrameworkFactoryWrapper.defaultRetryPolicy
    CuratorFrameworkFactoryWrapper.createAndConnect(quorumInfo) foreach { client =>
      client.reconfig().joining(quorumInfo.configString).forEnsemble()
      client.close()
    }
  }

}
