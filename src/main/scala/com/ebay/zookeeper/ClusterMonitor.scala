// Copyright (C) 2011-2012 the original author or authors.
// See the LICENCE.txt file distributed with this work for additional
// information regarding copyright ownership.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package com.ebay.zookeeper

import java.util.concurrent.{BlockingQueue, ConcurrentHashMap, LinkedBlockingDeque}

import com.ebay.zookeeper.client.CuratorFrameworkFactoryWrapper
import com.ebay.zookeeper.util.QuorumUtils
import org.slf4j.LoggerFactory

import scala.collection.JavaConversions._

class ClusterMonitor(quorum: ExtendedQuorumPeerMain, self: QuorumInfo) extends Runnable {

  import CuratorFrameworkFactoryWrapper._
  import QuorumUtils._

  val logger = LoggerFactory.getLogger(getClass)

  override def run(): Unit = {
    val members = quorum.getAllMembers.filterNot(_._2.serverId == self.serverId).toMap
    val unreachableQuorums = new ConcurrentHashMap[Int, QuorumInfo](members.size)
    val queue = new LinkedBlockingDeque[QuorumInfo](members.size)
    logger.info("[Server {}] checking status for members: {}", self.serverId, members)
    members.values foreach { member =>
      new Thread(new MonitorWorker(member, queue, unreachableQuorums)).start()
    }
    members foreach { entry =>
      queue.take()
    }
    if (unreachableQuorums.nonEmpty) {
      logger.info("[Server {}] find unreachable {}", self.serverId, unreachableQuorums)
    }
    if (quorum.isLeader && unreachableQuorums.nonEmpty) {
      // remove the unreachable members from the config
      logger.info("[Leader {}] remove unreachable members {}", self.serverId, unreachableQuorums)
      CuratorFrameworkFactoryWrapper.createAndConnect(self) foreach { client =>
        client.reconfig().leaving(unreachableQuorums.keySet().map(_.toString).toList).forEnsemble()
        logger.info("[Leader {}] successfully removed unreachable members {}", self.serverId, unreachableQuorums)
        client.close()
      }
    }

  }

  class MonitorWorker(quorumInfo: QuorumInfo,
                      queue: BlockingQueue[QuorumInfo],
                      unreachableQuorums: ConcurrentHashMap[Int, QuorumInfo]) extends Runnable {

    override def run(): Unit = {
      CuratorFrameworkFactoryWrapper.createAndConnect(quorumInfo) match {
        case Some(client) => client.close()
        case None => unreachableQuorums.put(quorumInfo.serverId, quorumInfo)
      }
      queue.put(quorumInfo)
    }
  }

}