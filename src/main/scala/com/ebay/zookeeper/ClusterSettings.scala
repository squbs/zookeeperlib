// Copyright (C) 2011-2012 the original author or authors.
// See the LICENCE.txt file distributed with this work for additional
// information regarding copyright ownership.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package com.ebay.zookeeper

import java.io.File
import java.net.InetAddress
import java.util.concurrent.TimeUnit

import com.typesafe.config.{Config, ConfigFactory}
import org.apache.zookeeper.server.quorum.QuorumPeer.LearnerType

import scala.collection.JavaConversions._
import scala.language.postfixOps
import scala.util.Try

case class ClusterSettings(servers: List[QuorumInfo], persistDir: File, initialDelay: Long, heartbeatInterval: Long)

object ClusterSettings {

  def fromConfig(config: Config): ClusterSettings = ClusterSettings(
    config.getConfigList("servers") map { c =>
      val host = InetAddress.getByName(c.getString("host"))
      QuorumInfo(
        host = host,
        quorumPort = Try(c.getInt("quorumPort")) getOrElse QuorumInfo.DEFAULT_QUORUM_PORT,
        electionPort = Try(c.getInt("electionPort")) getOrElse QuorumInfo.DEFAULT_ELECTION_PORT,
        clientPort = Try(c.getInt("clientPort")) getOrElse QuorumInfo.DEFAULT_CLIENT_PORT,
        learnerType = Try(c.getString("learnerType").toLowerCase) map {
          case "participant" => LearnerType.PARTICIPANT
          case "observer" => LearnerType.OBSERVER
        } getOrElse LearnerType.PARTICIPANT
      )
    } toList,
    new File(config.getString("persistDir")),
    config.getDuration("initialDelay", TimeUnit.MILLISECONDS),
    config.getDuration("heartbeatInterval", TimeUnit.MILLISECONDS)
  )
}

case class QuorumInfo(host: InetAddress,
                      quorumPort: Int = QuorumInfo.DEFAULT_QUORUM_PORT,
                      electionPort: Int = QuorumInfo.DEFAULT_ELECTION_PORT,
                      clientPort: Int = QuorumInfo.DEFAULT_CLIENT_PORT,
                      learnerType: LearnerType = LearnerType.PARTICIPANT) {

  lazy val serverId = (host.getHostAddress + quorumPort).hashCode

  lazy val connectionString = s"${host.getHostAddress}:$clientPort"

  lazy val configString = s"server.$serverId=${host.getHostAddress}:$quorumPort:$electionPort:$learnerType;$clientPort"

}

object QuorumInfo {

  lazy val DEFAULT_QUORUM_PORT = config.getInt("zookeeper.default_quorum_port")
  lazy val DEFAULT_ELECTION_PORT = config.getInt("zookeeper.default_election_port")
  lazy val DEFAULT_CLIENT_PORT = config.getInt("zookeeper.default_client_port")
  private lazy val config = ConfigFactory.load(QuorumInfo.getClass.getClassLoader)
}