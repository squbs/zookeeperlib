// Copyright (C) 2011-2012 the original author or authors.
// See the LICENCE.txt file distributed with this work for additional
// information regarding copyright ownership.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package com.ebay.zookeeper.client

import java.util.concurrent.TimeUnit

import com.ebay.zookeeper.QuorumInfo
import org.apache.curator.RetryPolicy
import org.apache.curator.framework.{CuratorFramework, CuratorFrameworkFactory}
import org.apache.curator.retry.RetryNTimes
import org.slf4j.LoggerFactory

object CuratorFrameworkFactoryWrapper {

  implicit lazy val defaultRetryPolicy = new RetryNTimes(DEFAULT_RETRIES, DEFAULT_RETRY_INTERVAL)
  val DEFAULT_RETRIES = 3
  val DEFAULT_RETRY_INTERVAL = 1000
  val CONNECTION_TIMEOUT = 5 * 1000
  val SESSION_TIMEOUT = 60 * 1000
  private val logger = LoggerFactory.getLogger(getClass)

  def createAndConnect(quorums: QuorumInfo*)(implicit retryPolicy: RetryPolicy): Option[CuratorFramework] = {
    val connectionString = quorums.map(_.connectionString).mkString(",")
    val client = CuratorFrameworkFactory.newClient(connectionString, retryPolicy)
    client.start()
    client.blockUntilConnected(CONNECTION_TIMEOUT, TimeUnit.MILLISECONDS) match {
      case true =>
        logger.info("Started zookeeper connection to {}", quorums)
        Some(client)
      case false =>
        logger.error("Failed to connect to {}", quorums)
        client.close()
        None
    }
  }
}
