// Copyright (C) 2011-2012 the original author or authors.
// See the LICENCE.txt file distributed with this work for additional
// information regarding copyright ownership.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package com.ebay.zookeeper

import java.io.ByteArrayInputStream
import java.util.Properties

import com.ebay.zookeeper.client.CuratorFrameworkFactoryWrapper
import org.apache.curator.framework.CuratorFrameworkFactory
import org.apache.curator.framework.api.CuratorWatcher
import org.apache.zookeeper.WatchedEvent
import org.apache.zookeeper.server.quorum.flexible.QuorumMaj
import org.slf4j.LoggerFactory

import scala.collection.JavaConversions._

object Client extends App {

  val logger = LoggerFactory.getLogger(getClass)

  var connectionString = "localhost:2791,localhost:2792,localhost:2793"

  val client = CuratorFrameworkFactory.newClient(connectionString, CuratorFrameworkFactoryWrapper.defaultRetryPolicy)

  client.start()
  client.blockUntilConnected()

  val watcher = new CuratorWatcher {
    override def process(event: WatchedEvent): Unit = {
      val connStr: String = client.getConfig.usingWatcher(this).forEnsemble()
      if (connStr != connectionString) {
        logger.info("Connection string changed to: {}", connStr)
        connectionString = connStr
      }
    }
  }

  connectionString = client.getConfig.usingWatcher(watcher).forEnsemble()

  private implicit def bytesToConnectionString(bytes: Array[Byte]): String = {
    val properties = new Properties
    properties.load(new ByteArrayInputStream(bytes))
    val newConfig = new QuorumMaj(properties)
    newConfig.getAllMembers.values() map { server =>
      server.addr.getAddress.getHostAddress + ":" + server.clientAddr.getPort
    } mkString ","
  }

  while (true) {
    Thread.sleep(10000)
  }
}
